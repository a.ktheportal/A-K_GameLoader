#!/usr/bin/python3

import tkinter as tk
from random import choice

app = tk.Tk()

letters = ["-","[","_","]","L","W","l","w"]
rip = ''.join([choice(letters) for i in range(10)])

label = tk.Label(app, text=f"This is another test {rip}. https://www.youtube.com/watch?v=ub82Xb1C8os")

label.pack()

app.mainloop()
